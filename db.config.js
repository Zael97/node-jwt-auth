const Sequelize = require('sequelize');
const config = require('./config');

const sequelize = new Sequelize(config.database, config.username, config.password, {
    host: config.host,
    port: config.port,
    dialect: 'postgres',
    pool: {
        min: parseInt(config.min),
        max: parseInt(config.max),
        idle: parseInt(config.idle),
        acquire: parseInt(config.acquire)
    },
    timestamp: config.timestamp
});



const db = {}

db.sequelize = sequelize;
db.Sequelize = Sequelize;
db.User = require('./models/user.model')(sequelize, Sequelize);
module.exports = db;
const { User } = require("../db.config");
const utils = require('../utils/utils');

exports.home = (req, res, next) => {
    res.render("home", { user: "Anthony" });
};

exports.smoothies = (req, res, next) => {
    res.render("smoothie");
};

exports.signup_get = (req, res, next) => {

};

exports.login_get = async (req, res, next) => {
    res.render("login");
};

exports.signup_post = async (req, res, next) => {
    const user = User.build(req.body);
    await user.save()
        .then((data) => {
            try {
                const from = 'anthony.munante.chavez@gmail.com';
                const to = data.email;
                const subject = 'email test';
                const token = utils.createToken(user.username);
                const html = `<html><h1>Welcome, click below to activate your account!</h1><a target='_blank' href='http://localhost:8000/${token}/activate'>Activate</a></html>`;
                utils.sendEmail(from, to, subject, html);
            } catch (err) {
                res.status(400).json({ err });
            }
            res.status(201).json({ data: data })
        })
        .catch(err => res.status(400).json({ errors: utils.handleErrors(err) }));
};

const maxAge = 3 * 24 * 60 * 60;
exports.login_post = async (req, res, next) => {
    const { username, password } = req.body;
    try {
        const user = await User.login(username, password);
        res.cookie('jwt', utils.createToken(user.username), { httpOnly: true, maxAge: maxAge * 1000 });
        res.status(200).json({ user: user.username });
    } catch (err) {
        res.status(400).json({ error: utils.handleErrors(err) })
    }
};
exports.me = async (req, res, next) => {
    try {
        const token = utils.extractToken(req);
        const user = await User.findByUsername(utils.verify(token));
        res.status(200).json({ user });
    } catch (err) {
        res.status(400).json({ error: utils.handleErrors(err) });
    }
}
exports.activate_account = async (req, res, next) => {
    const token = req.params.token;
    try {
        const username = utils.verify(token);
        const user = await User.findByUsername(username);
        user.active = true;
        user.save();
    } catch (err) {
        res.status(400).json({ error: utils.handleErrors(err) });
    }

}
/*
exports.set_cookies = async (req, res, next) => {
    //res.setHeader('Set-Cookie', 'newUser=true')
    res.cookie('newUser', false);
    //(secure: true)set access only if https, (httpOnly: true)you can not access the cookies with front JavaScript just in the back-end
    res.cookie('isEmployee', true, { maxAge: 1000 * 60 * 60 * 24, httpOnly: true });
    res.send('You got the cookie');
}
exports.read_cookies = async (req, res, next) => {
    const cookies = req.cookies;
    console.log(cookies);
    res.json(cookies);
}*/
const app = require('./app');
const port = process.env.PORT || 9000;

app.listen(port, () => {
    console.log(`Server working on port ${port}.Press Ctrl+C to stop.`);
});
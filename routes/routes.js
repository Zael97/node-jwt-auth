const { Router } = require('express');
const router = Router();
const controllers = require('../controllers/controllers');
router.get('/home', controllers.home);
router.get('/smoothies', controllers.smoothies);
router.get('/signup', controllers.signup_get);
router.get('/login', controllers.login_get);
router.post('/signup', controllers.signup_post);
router.post('/login', controllers.login_post);
router.get('/me', controllers.me);
router.get('/:token/activate', controllers.activate_account)
//router.get('/set-cookies', controllers.set_cookies);
//router.get('/read-cookies', controllers.read_cookies);
module.exports = router;
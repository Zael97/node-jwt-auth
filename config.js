require('dotenv').config();

const { DB_PORT, DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME, DB_MIN, DB_MAX, DB_IDLE, DB_ACQUIRE, DB_DIALECT } = process.env;

module.exports = {
    "username": DB_USERNAME,
    "password": DB_PASSWORD,
    "database": DB_NAME,
    "host": DB_HOST,
    "min": DB_MIN,
    "max": DB_MAX,
    "idle": DB_IDLE,
    "acquire": DB_ACQUIRE,
    "port": DB_PORT
}
const bcrypt = require('bcrypt');

'use strict';
module.exports = (sequelize, Sequelize) => {
    const Op = Sequelize.Op;
    const User = sequelize.define('users', {
        id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            primaryKey: true,
            allowNull: false
        },
        dni: {
            type: Sequelize.STRING,
            required: true,
            allowNull: false,
            required: true,
            unique: true,
            validate: {
                notNull: {
                    msg: 'null is not allowed'
                },
                notEmpty: {
                    msg: 'empty string is not allowed'
                },
                is: {
                    args: ["^[0-9]{8}$", 'i'],
                    msg: 'dni format invalid'
                }
            }
        },
        nombres: {
            type: Sequelize.STRING,
            required: true,
            allowNull: false,
            required: true,
            validate: {
                notNull: {
                    msg: 'null is not allowed'
                },
                notEmpty: {
                    msg: 'empty string is not allowed'
                }
            }
        },
        apellidos: {
            type: Sequelize.STRING,
            required: true,
            allowNull: false,
            validate: {
                notNull: {
                    msg: 'null is not allowed'
                },
                notEmpty: {
                    msg: 'empty string is not allowed'
                }
            }
        },
        direccion: {
            type: Sequelize.STRING,
            allowNull: true,
            validate: {
                notEmpty: {
                    msg: 'empty string is not allowed'
                }
            }
        },
        celular: {
            type: Sequelize.STRING,
            required: true,
            allowNull: false,
            validate: {
                notNull: {
                    msg: 'null is not allowed'
                },
                notEmpty: {
                    msg: 'empty string is not allowed'
                },
                is: {
                    args: ["^9[0-9]{8}$", 'i'],
                    msg: 'number not valid'
                }
            }
        },
        email: {
            type: Sequelize.STRING,
            required: true,
            unique: true,
            allowNull: false,
            validate: {
                notNull: {
                    msg: 'null is not allowed'
                },
                isEmail: {
                    msg: 'email not valid'
                },
                notEmpty: {
                    msg: 'empty string is not allowed'
                }
            }
        },
        username: {
            type: Sequelize.STRING,
            required: true,
            unique: true,
            allowNull: false,
            validate: {
                notNull: {
                    msg: 'null is not allowed'
                },
                isAlpha: {
                    msg: 'only letters allowerd'
                },
                notEmpty: {
                    msg: 'empty string is not allowed'
                }
            }
        },
        password: {
            type: Sequelize.STRING,
            required: true,
            minLength: 6,
            allowNull: false,
            validate: {
                notNull: {
                    msg: 'null is not allowed'
                },
                notEmpty: {
                    msg: 'empty string is not allowed'
                }
            }
        },
        active: {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            required: false,
            allowNull: false
        }
    }, {
        schema: 'public'
    });

    User.beforeCreate(async user => {
        user.email = user.email.toLowerCase();
        const salt = await bcrypt.genSalt();
        user.password = await bcrypt.hash(user.password, salt);
        //console.log(decrypt(JSON.parse(user.password)));
    });
    //class level method
    User.login = async function (username, password) {
        const user = await this.findOne({
            where: {
                username: username
            }
        });
        if (user) {
            if (!user.active) {
                throw Error('inactive account');
            }
            const auth = await bcrypt.compare(password, user.password);
            if (auth) {
                return user;
            }
            throw Error('incorrect password');
        }
        throw Error('incorrect username');
    }
    User.findByUsername = async function (username) {
        const user = await this.findOne({
            where: {
                username: username
            }
        })
        if (user) {
            return user;
        }
        throw Error('incorrect username');
    }
    return User;
}



const jwt = require('jsonwebtoken');
const mailgun = require('mailgun-js');
const maxAge = 3 * 24 * 60 * 60;

/**
 * 
 * @param {*} id 
 * @returns 
 */
exports.createToken = username => {
    return jwt.sign({ username }, 'secret key', {
        expiresIn: maxAge
    })
}

/**
 * 
 * @param {*} err 
 * @returns 
 */
exports.handleErrors = err => {
    //console.log('ERROR:', err.code);
    let errors = { dni: '', nombres: '', apellidos: '', direccion: '', celular: '', email: '', username: '', password: '', token: '', active: '' }
    if (err.message.includes('Validation error')) {
        Object.values(err.errors).forEach(({ message, path }) => {
            errors[path] = message;
        })
    }
    if (err.message === 'incorrect password') {
        errors.password = 'that password is incorrect';
    }
    if (err.message === 'incorrect username') {
        errors.username = 'that username is no registered';
    }
    if (err.message === 'token not found') {
        errors.token = 'the token was not found';
    }
    if (err.message === 'invalid token') {
        errors.token = 'the token is invalid';
    }
    if (err.message === 'inactive account') {
        errors.active = 'account is not active'
    }
    return errors;
}
/**
 * 
 * @param {*} token 
 * @returns 
 */
exports.verify = token => {
    try {
        const encoded = jwt.verify(token, 'secret key');
        return encoded.username;
    } catch (err) {
        throw Error('invalid token');
    }
}
/**
 * 
 * @param {*} req 
 * @returns 
 */
exports.extractToken = req => {
    try {
        const token = req.headers['authorization'].split(' ')[1];
        return token;
    } catch (err) {
        throw Error('token not found');
    }
}

const apiKey = '11170a172973830aeab31de917839bf4-4de08e90-d208d955';
const domain = 'sandbox5e7e5b88ac0d4533a13ce4d734d34038.mailgun.org';
const mg = mailgun({ apiKey, domain });
exports.sendEmail = (from, to, subject, html, text) => {
    const data = { from, to, subject, html, text }
    try {
        const body = mg.messages().send(data);
        return body;
    } catch (err) {
        throw Error('error sending email');
    }
}
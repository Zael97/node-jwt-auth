const controllers = require("../../controllers/controllers");
const nodeMocksHttp = require("node-mocks-http");
const db = require("../../db.config");
const userResponseMockData = require("../mock-data/user-response-mock-data.json");
const userRequestMockData = require("../mock-data/user-request-mock-data.json");
const User = db.User;
let req, res, next;
beforeEach(() => {
    req = nodeMocksHttp.createRequest();
    res = nodeMocksHttp.createResponse();
    next = jest.fn();
});

describe("User controller test suite", () => {
    test("User.create must be called", () => {
        User.create = jest.fn();
        const resolvePromise = Promise.resolve(userResponseMockData);
        User.create.mockReturnValue(resolvePromise);
        controllers.signup_post(req, res, next);
        expect(User.create).toBeCalled();
    });
    test("User.create must be a function", () => {
        expect(typeof User.create).toBe("function");
    });
    test("Must have an status code of 201", async () => {
        User.create = jest.fn();
        req.body = userRequestMockData;
        const resolvePromise = Promise.resolve(userResponseMockData.data);
        await User.create.mockReturnValue(resolvePromise);
        await controllers.signup_post(req, res, next);
        expect(res.statusCode).toBe(201);
        expect(res._isEndCalled()).toBeTruthy();
    });
    test("Must return json body", async () => {
        User.create = jest.fn();
        req.body = userRequestMockData;
        const resolvePromise = Promise.resolve(userResponseMockData);
        await User.create.mockReturnValue(resolvePromise);
        await controllers.signup_post(req, res, next);
        expect(res._getJSONData()).toStrictEqual({ data: userResponseMockData });
        expect(res._isEndCalled()).toBeTruthy();
    });
    test("next must be called if error", async () => {
        const rejectedPromise = Promise.reject({ error: 'something was wrong' });
        User.create.mockReturnValue(rejectedPromise);
        await controllers.signup_post(req, res, next);
        expect(next).toBeCalled();
    });
});

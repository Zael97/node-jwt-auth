const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const db = require("./db.config");
const routes = require('./routes/routes');
const app = express();
const cookieParser = require('cookie-parser');
const sync = true;
const corsOptions = {
    origin: "*",
};

app.use(bodyParser.json());
app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors(corsOptions));
app.use(express.json());
app.use(routes);
app.use(cookieParser());
app.use((err, req, res, next) => {
    //console.error('Error:', Object.values(err.errors));
    res.status(400).json({ error: err.message });
})
// Set template engine: pug
app.set("view engine", "pug");

db.sequelize
    .sync({ force: sync })
    .then(`Drop and sync database: ${sync}`)
    .catch("Something went wrong with drop and sync the database");
//Database authentication verifier
db.sequelize.authenticate().then('Authentication Ok!').catch('Something wrong with authentication!');


module.exports = app;
